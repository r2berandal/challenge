'use strict';
exports.calulateExpenses = (data) => {
    if (data['length'] && data['length'] > 0) {
        
        //Calculate average
        let average = 0;
        data.forEach(datum => {
            average += parseFloat(datum['expense']);
        });
        average = average / data['length'];

        //calculate difference per student's total expenses
        data.map(datum => {
            datum['expense'] = parseFloat(datum['expense']);
            let diff = average - datum['expense'];
            datum['deltaFromAvg'] = diff;
            return datum;
        });
    }
    return data;
}