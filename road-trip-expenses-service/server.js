let express = require('express'),
  app = express(),
  port = process.env.PORT || 8080,
  bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.setHeader("content-type", "application/json");
  res.setHeader('X-Requested-With', 'XMLHttpRequest');
  next();
});

let routes = require('./api/routes/generic-routes');
routes(app);

app.listen(port);
console.log('todo list RESTful API server started on: ' + port);