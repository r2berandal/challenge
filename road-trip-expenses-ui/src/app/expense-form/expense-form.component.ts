import { Component, OnInit } from '@angular/core';
import { ExpenseFormService } from './expense-form.service';
import { ExpenseFormModel } from './expense-form.data-model';

@Component({
  selector: 'app-expense-form',
  templateUrl: './expense-form.component.html',
  styleUrls: ['./expense-form.component.scss']
})
export class ExpenseFormComponent implements OnInit {
  modelForExpense: ExpenseFormModel = { name: null, expense: null, deltaFromAvg: null };
  studentArr: ExpenseFormModel[] = [];
  
  constructor(private expenseService: ExpenseFormService) { }

  ngOnInit(): void {
    this.addNewStudent();
  }

  private callback(result: ExpenseFormModel[]) {
    this.studentArr = result;
  }

  private createNewExpenseObj() {
    return JSON.parse(JSON.stringify(this.modelForExpense));
  }

  public keyPress(event){
    let charCode = (event.which) ? event.which : event.keyCode;
    if (charCode != 46 && charCode > 31
      && (charCode < 48 || charCode > 57)) {
      event.preventDefault();
      return false;
    }
    return true;
  }

  public addNewStudent() {
    this.studentArr.push(this.createNewExpenseObj());
  }

  public removeStudent(name: string, expense: number) {
    let resultOfRemoval = this.studentArr.filter(student => student['name'] !== name && student['expense'] !== expense);
    this.studentArr = resultOfRemoval.length > 0 ? resultOfRemoval : [this.createNewExpenseObj()];
  }

  public async calculate() {
    let test = await this.expenseService.calculateExpensesPost(this.studentArr);
    test.subscribe((results: ExpenseFormModel[]) => {
      this.callback(results);
    });
  }

}
