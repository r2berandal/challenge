import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ExpenseFormModel } from './expense-form.data-model';

@Injectable({
  providedIn: 'root'
})
export class ExpenseFormService {
  constructor(private http: HttpClient) { }

  public async calculateExpensesPost(data: ExpenseFormModel[]) {
    let url = 'http://localhost:8080/calculate';
    return await this.http.post(url, data);
  }
}
