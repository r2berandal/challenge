export interface ExpenseFormModel {
  name: string;
  expense: number;
  deltaFromAvg: number;
}
